using System;
using UnityEngine;

namespace Ranraran.GameDev.Chap2
{
    public class ControlObjectMovementOnXZPlaneUsingTFGH : StepMovement
    {
        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.F))
            {
                MoveLeft();
            }
            else if (Input.GetKey(KeyCode.H))
            {
                MoveRight();
            }
            else if (Input.GetKey(KeyCode.T))
            {
                MoveForward();
            }
            else if (Input.GetKey(KeyCode.G))
            {
                MoveBackward();
            }

            if (Input.GetKey(KeyCode.F))
            {
                MoveRight();
            }
            else if (Input.GetKey(KeyCode.H))
            {
                MoveLeft();
            }
            else if (Input.GetKey(KeyCode.T))
            {
                MoveBackward();
            }
            else if (Input.GetKey(KeyCode.G))
            {
                MoveForward();
            }
        }
    }
}