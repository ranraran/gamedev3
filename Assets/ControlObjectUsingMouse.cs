using UnityEngine;

namespace Ranraran.GameDev.Chap2
{
    public class ControlObjectUsingMouse : MonoBehaviour
    {
        private Vector3 m_MousePreviousPosiotion;
        public float m_MouseDeltaVectorScaling = 0.5f;

        void Start()
        {
            
        }

        void Update()
        {
            Vector3 mouseCurrentPos = Input.mousePosition;
            Vector3 mouseDeltaVector = Vector3.zero;
            mouseDeltaVector = (mouseCurrentPos - m_MousePreviousPosiotion).normalized;

            if (Input.GetMouseButton(0))
            {
                this.transform.Translate(mouseDeltaVector*m_MouseDeltaVectorScaling,Space.World);
            }
            this.transform.Translate(0,0,Input.mouseScrollDelta.y*m_MouseDeltaVectorScaling,Space.World);
            m_MousePreviousPosiotion = mouseCurrentPos;
        }
    }
}