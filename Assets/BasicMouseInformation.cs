using System;
using UnityEngine;
using UnityEngine.UI;

namespace Ranraran.GameDev.Chap2
{
    public class BasicMouseInformation : MonoBehaviour
    {
        public Text m_TextMousePosition;

        private void Update()
        {
            m_TextMousePosition.text = Input.mousePosition.ToString();
        }
    }
}