using System;
using UnityEngine;

namespace Ranraran.GameDev.Chap2
{
    public class ControlObjectMovementOnXZPlaneUsingArrowKeys : StepMovement
    {
       void Update()
        {
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                MoveLeft();
            }
            else if (Input.GetKey(KeyCode.RightArrow))
            {
                MoveRight();
            }
            else if (Input.GetKey(KeyCode.UpArrow))
            {
                MoveForward();
            }
            else if (Input.GetKey(KeyCode.DownArrow))
            {
                MoveBackward();
            }
        }
    }
}